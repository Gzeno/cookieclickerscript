import os
import ImageGrab
import time

def screenGrab(): #take a screenshot for reference
	box = (0,85,1350,725) #box gets an uninitialized tuple
	im = ImageGrab.grab() # returns RGB image
	im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')
	
def main():
	screenGrab()
	
if __name__ == '__main__':
	main()

