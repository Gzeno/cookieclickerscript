import win32api, win32con
import os
import ImageGrab
import time

#first pixel out of the play area so it is beginning of left corner box - 1
xpad = -1
ypad = 84

def screenGrab(): #take a screenshot for reference
        box = (xpad+1,ypad+1,xpad+1351,ypad +736) #box gets an uninitialized tuple
        im = ImageGrab.grab() # returns RGB image
        im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) + '.png', 'PNG')

def leftClick(): #click once on the mouse
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,0,0)
        time.sleep(.1)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,0,0)
        #print "Click." #report that click was completed
        
def moveMouse(cord):
        win32api.SetCursorPos((xpad+cord[0], ypad+cord[1]))

def getCord():
        x,y = win32api.GetCursorPos()
        x = x-xpad
        y = y-ypad
        print x,y

def clickCookie():
        moveMouse((236,248))
        leftClick()
        #time.sleep(.1)
        
def main():
        while True:
                clickCookie()
        
if __name__ == '__main__':
        main()
        


